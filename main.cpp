#include <iostream>
#include <thread>

void do_something(int &i) {
    std::cout << i << ", ";
}

class background_task {
public:
    void operator()() const {
        do_something();
        do_something_else();
    }

private:
    void do_something_else() const {
        std::cout << __PRETTY_FUNCTION__ << std::endl;
    }

    void do_something() const {
        std::cout << __PRETTY_FUNCTION__ << std::endl;
    }
};

struct func {
    int &var;

    func(int &var_) : var(var_) {}

    void operator()() {
        for (int i = 0; i < 10; ++i) {
            do_something(i);
        }
    }
};

void oops() {
    /**
    * @brief 当oops()函数执行完成时，新线程 my_thread 中的函数my_func
    * 可能还在运行。 如果线程还在运行，它就会去调用do_something(i)函数，
    * 这时就会访问已经销毁的变量。如同一个单线程程序——允许在函数完
    * 成后继续持有局部变量的指针或引用；这种情况发生时，错误并不明显，会使多线程更容易出错。
    */
    int some_local_state = 0;
    func my_func(some_local_state);
    std::thread my_thread{my_func};
    /**
    * @brief detach模式
    */
    my_thread.detach();
}

int main() {
    //std::thread my_thread_1{background_task()};
    //my_thread_1.join();
    /**
    * @brief 下面这种写法是错误的
    * @code std::thread my_thread_2(background_task());@endcode
    * 这里相当与声明了一个名为my_thread的函数，这个函数带有一个参数
    * 返回一个std::thread对象的函数，而非启动了一个线程。
    * 函数指针指向没有参数并返回background_task对象的函数
    * 使用lambda或者花括号初始化可以避免这种情况
    * @code
    * // lambda
    * std::thread my_thread_3([](){
    *     do_something();
    *     do_something_else();
    * });
    * // 花括号
    * std::thread my_thread_1{background_task()};
    * @endcode
    */

    oops();
    return 0;
}
